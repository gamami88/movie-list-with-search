import React from "react";
import "./footer.css";
const Footer = () => {
  return (
    <div className="footer-copyright">
      <div>
        <p> ©Gamami {new Date().getFullYear()} Campflix </p>
      </div>
    </div>
  );
};

export default Footer;
