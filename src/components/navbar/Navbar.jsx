import React, { useState, useEffect } from "react";
import "./navbar.css";
import logo from "../../assets/logo.png";
import { Link } from "react-router-dom";

const Navbar = (props) => {
  const onChange = (field, value) => {
    console.log(value);
    props.search.action(value);
  };

  return (
    <div className="navbar">
      <div className="navbar-links">
        <div className="navbar-links_logo">
          <img src={logo} alt="logo" />
          <Link to="/">
            <h1>Campflix</h1>
          </Link>
        </div>
      </div>
      <div className="navbar-links_container">
        <input
          type="text"
          onChange={(e) => onChange("full_name", e.currentTarget.value)}
          placeholder="Search Item Here"
          autoFocus={true}
        />
      </div>
      &emsp;
      <div className="navbar-links_logo h3">
        <h3>Ghozali Amami</h3>
      </div>
    </div>
  );
};

export default Navbar;
