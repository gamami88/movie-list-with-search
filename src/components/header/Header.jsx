import React, { useState, useEffect } from "react";
import "./header.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import note_found from "../../assets/note_found.png";
import axios from "axios";
import profile_banner from "../../assets/banner.png";
import { Navbar } from "../../components";

const Header = () => {
  const [Listfilm, setFilm] = useState([]);
  const [Search, setSearch] = useState("girls");
  useEffect(() => {
    axios
      .get(`http://api.tvmaze.com/search/shows?q=${Search}`)
      .then((result) => {
        setFilm(result.data);
      });
  }, [Search]);
  const searchLoading = (display) => {
    if (display) {
      setSearch(display);
    } else {
      setSearch("girls");
    }
  };

  var settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    initialSlide: 0,
    swipeToSlide: true,
    responsive: [
      {
        breakpoint: 1160,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          swipeToSlide: true,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 550,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 470,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: true,
        },
      },
    ],
  };
  return (
    <>
      <Navbar search={{ action: searchLoading }} />
      <div className="header section__padding">
        <div className="profile section__padding">
          <div className="profile-top">
            <div className="profile-banner">
              <img src={profile_banner} alt="banner" />
            </div>
          </div>
        </div>
        <div className="header-slider">
          <h1>All Movies</h1>
          {Listfilm.length ? (
            <Slider {...settings} className="slider">
              {Listfilm.map((mov, index) => (
                <div key={mov.show.id} className="slider-card">
                  <div className="slider-img">
                    {mov.show.image ? (
                      <img src={mov.show.image.medium} alt="" />
                    ) : (
                      <img src={note_found} />
                    )}
                  </div>
                  <a href={mov.show.url}>
                    <p>{mov.show.name}</p>
                  </a>
                  <p>
                    {mov.show.rating.average
                      ? "⭐️" + mov.show.rating.average
                      : "No Rating"}
                  </p>
                </div>
              ))}
            </Slider>
          ) : (
            <img src={note_found} />
          )}
        </div>
      </div>
    </>
  );
};

export default Header;
