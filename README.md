<div align="center">
      <h1> <img src="https://imgkub.com/image/banner.FbZO" width="500px"><br/></h1>
     </div>

# README

Hi friends, this is a test for junior frontend developer.

Please follow the steps and goal to be successfull candidate.

# install dependencies

$ npm install

# serve with host at localhost:3000

$ npm start

```

# Screenshot
!["React Movie App"](https://imgkub.com/image/screenshot.FJHX)


# Resource

    Google font: https://fonts.google.com/

    FontAwesome : https://fontawesome.com/



### Task
- [x] Home page

```
